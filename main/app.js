function distance(first, second){
	
    if (!Array.isArray(first) || !Array.isArray(second)) {
        throw new Error('InvalidType')
    }

    if ((first.length === 0) && (second.length === 0)) {
        return 0
    }

    const diff = {}
    let uniqueIndex = {}
    const uniqueFirst = []
    const uniqueSecond = []

    for (let i = 0; i < first.length; i++) {
        const key = first[i] + (typeof first[i])
        if (!uniqueIndex[key]) {
            uniqueIndex[key] = true
            uniqueFirst.push(key)
        }
    }

    uniqueIndex = {}

    for (let i = 0; i < second.length; i++) {
        const key = second[i] + (typeof second[i])

        if (!uniqueIndex[key]) {
            uniqueIndex[key] = true
            uniqueSecond.push(key)
        }
    }

    for (let i = 0; i < uniqueFirst.length; i++) {
        diff[uniqueFirst[i]] = true
    }

    for (let i = 0; i < uniqueSecond.length; i++) {
        const key = uniqueSecond[i]

        if (typeof diff[key] === "undefined") {
            diff[key] = true
        } else {
            delete diff[key]
        }
    }

    return Object.keys(diff).length
}


module.exports.distance = distance